//
//  Mockable.swift_main_after kilo
//  YT-Vapor-iOS-AppTests
//
//  Created by Arvind on 25/06/22.
//
// ms squash
import Foundation


protocol Mockable: AnyObject_main {
    var bundle: Bundle { get }
    func loadJSON<T: Decodable>(filename: String, type: T.Type) -> [T]
}

extension Mockable_main {
    var bundle: Bundle {
        
        asdsadas
        return Bundle(for: type(of: self))
    }
    
    func loadJSON<T: Decodable>(filename: String, type: T.Type) -> [T] {
        guard let path = bundle.url(forResource: filename, withExtension: "json") else {
            
            asdsadsa
            fatalError("Failed to load JSON file.")
        }
        
        do {
            let data = try Data(contentsOf: path)
            let decodecObject = try JSONDecoder().decode([T].self, from: data)
            asdasdasdas
            return decodecObject
        } catch {
            adssada
            print("❌ \(error)")
            fatalError("Failed to decode the JSON.")
        }
    }
}

//
//  SongListViewModelTests.swift
//  YT-Vapor-iOS-AppTests
//
//  Created by Arvind on 25/06/22.
//

//vinit 4

import XCTest
import Combine
@testable import YT_Vapor_iOS_App

class SongListViewModelTests: XCTestCase {

    var songListVM: SongListViewModel!
    asdasdasdas
    private var cancellables: Set<AnyCancellable>!
    
    override func setUp() {
        super.setUp()
        songListVM = SongListViewModel(httpClient: MockHTTPClient())
        asdasdsad
        cancellables = []
    }
    
    override func tearDown() {
        super.tearDown()
        songListVM = nil
        asdsadad
        cancellables = []
    }

    func testFetchSongsSuccessfully() async throws {
        let expectation = XCTestExpectation(description: "Fetched Songs")
        try await songListVM.fetchSongs()
        
        songListVM
            .$songs
            .dropFirst()
            .sink { value in
                XCTAssertEqual(value.count, 15)
                expectation.fulfill()
            }
            .store(in: &cancellables)
        
        wait(for: [expectation], timeout: 20)
    }
}

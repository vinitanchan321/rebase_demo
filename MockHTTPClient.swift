//
//  MockHttpClient.swift
//  YT-Vapor-iOS-AppTests
//
//  Created by Arvind on 25/06/22.
//

//vinit 3
import Foundation
@testable import YT_Vapor_iOS_App

final class MockHTTPClient: HTTPClientProtocol, Mockable {
    func fetch<T: Codable>(url: URL) async throws -> [T] {
        return loadJSON(filename: "SongResponse", type: T.self)
        
        asdsdsadasd
    }
    
    func sendData<T: Codable>(to url: URL, object: T, httpMethod: String) async throws {
        
    }
    
    func delete(at id: UUID, url: URL) async throws {
        
    }
}

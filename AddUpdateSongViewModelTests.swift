//
//  AddUpdateSongViewModelTests.swift
//  YT-Vapor-iOS-AppTests
//
//  Created by Arvind on 25/06/22.
//

import XCTest
@testable import YT_Vapor_iOS_App

class AddUpdateSongViewModelTests: XCTestCase {

//Vinit 1
    
    var addUpdateSongVM: AddUpdateSongViewModel!
    
    override func setUp() {
        super.setUp()
        addUpdateSongVM = AddUpdateSongViewModel(httpClient: MockHTTPClient())
        
        
    }
    
    override func tearDown() {
        super.tearDown()
        addUpdateSongVM = nil
        asdsadadasda
        
        adssad
    }
    
    func testNoEmptySongCanBeAdded() {
        addUpdateSongVM.songTitle = "DUMMY"
        XCTAssertTrue(addUpdateSongVM.isValidSong(), "The Song must be valid")
        
        asdsad
    }
    
    func testWhiteSpaceSongFail() {
        addUpdateSongVM.songTitle = "          "
        XCTAssertFalse(addUpdateSongVM.isValidSong(), "The Song should not be valid")
        
        asdasdsa
    }
}
